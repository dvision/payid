stages:
  - build
  - build-tar
  - copy-tar
  - deploy
  - test

variables:
  POSTGRES_USER: postgres
  POSTGRES_PASSWORD: password
  POSTGRES_DB: database_development
  POSTGRES_HOST_AUTH_METHOD: trust
  DB_HOSTNAME: postgres

  # variables for image build
  IMAGE_NAME: payid
  VERSION: 0.0.1
  APPLICATION_TAR: payid-0.0.1.tar
  INSTANCE_NAME: multinode-gce-4zc4

include:
  - template: Jobs/Build.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab-foss/blob/master/lib/gitlab/ci/templates/Jobs/Build.gitlab-ci.yml  

services:
  - postgres:12

gravity_build:
  image: gitlab/dind
  stage: build-tar
  services:
    - docker:dind
  before_script:
    - apt-get -y install git sudo bash curl
    - export TRIVY_VERSION=$(wget -qO - "https://api.github.com/repos/aquasecurity/trivy/releases/latest" | grep '"tag_name":' | sed -E 's/.*"v([^"]+)".*/\1/')
    - echo $TRIVY_VERSION
    - wget --no-verbose https://github.com/aquasecurity/trivy/releases/download/v${TRIVY_VERSION}/trivy_${TRIVY_VERSION}_Linux-64bit.tar.gz -O - | tar -zxvf -

  tags:
    - docker # We need docker, so run this task on shared runner
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - curl https://get.gravitational.io/telekube/bin/7.0.13/linux/x86_64/tele -o /usr/bin/tele && chmod 755 /usr/bin/tele # install gravity

    - echo $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG  # $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG
    - echo $CI_COMMIT_TAG
    - |
      if [ "$CI_COMMIT_TAG" != "" ]; then
        # Convert tag v0.0.1 to 0.0.1 version for using in chart name
        VERSION=`echo "$CI_COMMIT_TAG" | sed -r 's/v//g'`
        TAG=$VERSION
        CI_IMAGE_TAG=$CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
      else
        TAG=$CI_COMMIT_SHA
        CI_IMAGE_TAG=$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG
      fi
    - "sed -i \"s/version:.*$/version: $VERSION/g\" `pwd`/charts/$IMAGE_NAME/Chart.yaml"
    - "sed -i \"s/imageTag:.*$/imageTag: $TAG/g\" `pwd`/charts/$IMAGE_NAME/values.yaml"
    - docker pull $CI_IMAGE_TAG
    - docker tag $CI_IMAGE_TAG dvision/$IMAGE_NAME:$TAG
    - tele build charts/$IMAGE_NAME/
    - ./trivy --exit-code 0 --cache-dir .trivycache/ --no-progress --format template --template "@contrib/gitlab.tpl" -o gl-container-scanning-report.json $CI_IMAGE_TAG
    # Print report
    - ./trivy --exit-code 1 --cache-dir .trivycache/ --no-progress --severity HIGH $CI_IMAGE_TAG
    # Fail on high and critical vulnerabilities
    - ./trivy --exit-code 1 --cache-dir .trivycache/ --severity CRITICAL --no-progress $CI_IMAGE_TAG

  cache:
    paths:
      - .trivycache/
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
    - if: $CI_COMMIT_TAG != null

  artifacts:
    when: always
    paths:
      - "./$IMAGE_NAME-*.tar"
    expire_in: 10 hours
    reports:
      container_scanning: gl-container-scanning-report.json

copy_application:
  stage: copy-tar
  image: gcr.io/google.com/cloudsdktool/cloud-sdk:alpine
  script:
    - |
      if [ "$CI_COMMIT_TAG" != "" ]; then
        # Convert tag v0.0.1 to 0.0.1 version for using in chart name
        TAG=`echo "$CI_COMMIT_TAG" | sed -r 's/v//g'`
        APPLICATION_TAR=$IMAGE_NAME-$TAG.tar
        SUBDIR=tags/
      fi
    - echo $GCP_SERVICE_KEY > ${HOME}/gcloud-service-key.json
    - gcloud auth activate-service-account --key-file ${HOME}/gcloud-service-key.json
    - gcloud config set project $GCP_PROJECT_ID
    - gsutil -m cp $APPLICATION_TAR gs://bopp-dev-gravity/cicd/$SUBDIR
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
    - if: $CI_COMMIT_TAG != null
  allow_failure: true

deploy_application:
  stage: deploy
  image: gcr.io/google.com/cloudsdktool/cloud-sdk:alpine
  script:
    - echo $GCP_SERVICE_KEY > ${HOME}/gcloud-service-key.json
    - gcloud auth activate-service-account --key-file ${HOME}/gcloud-service-key.json
    - gcloud config set project $GCP_PROJECT_ID
    - gcloud compute ssh --zone europe-west2-c $INSTANCE_NAME -- "gsutil cp gs://bopp-dev-gravity/cicd/$APPLICATION_TAR . && sudo gravity app upgrade $IMAGE_NAME $APPLICATION_TAR --set registry=leader.telekube.local:5000/"
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
  allow_failure: true

lint:
  stage: test
  image:
    name: node:12
  before_script:
    - rm -rf .npm
    - npm i --cache .npm --prefer-offline --no-audit --progress=false
  script:
    - npm run lintNoFix

code coverage:
  stage: test
  image:
    name: node:12
  before_script:
    - rm -rf .npm
    - npm i --cache .npm --no-audit --progress=false --prefer-offline -g nyc codecov
    - npm i --cache .npm --no-audit --progress=false --prefer-offline
  script:
    - npm run build
    - nyc npm test
    - mkdir coverage
    - nyc report --reporter=text-lcov > coverage/coverage.json
    - codecov

link checker:
  stage: test
  image:
    name: golang:1.14-alpine
  before_script:
    - apk add git
    - export GO111MODULE=on
    - go get -u github.com/raviqqe/liche
  script:
    - liche -r ${CI_PROJECT_DIR}
  allow_failure: true
