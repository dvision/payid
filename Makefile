IMAGE_NAME=payid
REGISTRY=dvision
VERSION=0.0.1

build:
	go build -o ./bin/${IMAGE_NAME} ./cmd/main.go

image:
	docker build -t $(REGISTRY)/${IMAGE_NAME}:$(VERSION) -f ./Dockerfile . 
